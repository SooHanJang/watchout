package madcat.studio.watchout.record;

import java.util.ArrayList;

import madcat.studio.watchout.constants.Constants;
import madcat.studio.watchout.main.R;
import madcat.studio.watchout.utils.Util;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class RecordActivity extends Activity {
	private Context mContext = null;
	
	private LinearLayout mRecordLayout = null;
	private ImageView mLeftBackground = null;
	private ImageView mRightBackground = null;
	
	private int mScreenWidth = 0;
	private int mScreenHeight = 0;
	
	private Button mRecordTitle = null;
	
	private Button mRecordDeath = null;
	private Button mRecordTime = null;
	
	private Button mBackwardBtn = null;
	private Button mForwardBtn = null;
	
	private int mCurrentLevel = 0;
	
	private ArrayList<Integer> mDeathHighScoreList = null;
	private ArrayList<Long> mTimeHighScoreList = null;
	
	private boolean mEventFlag = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.record);
		
		mContext = this;
		
		mRecordLayout = (LinearLayout)findViewById(R.id.recordLayout);
		mLeftBackground = (ImageView)findViewById(R.id.recordLeftBackground);
		mRightBackground = (ImageView)findViewById(R.id.recordRightBackground);
		
		mRecordTitle = (Button)findViewById(R.id.recordTitle);
		
		mRecordDeath = (Button)findViewById(R.id.deathHighScore);
		mRecordTime = (Button)findViewById(R.id.timeHighScore);
		
		mDeathHighScoreList = new ArrayList<Integer>();
		mTimeHighScoreList = new ArrayList<Long>();
		
		mBackwardBtn = (Button)findViewById(R.id.backwardRecord);
		mForwardBtn = (Button)findViewById(R.id.forwardRecord);
		
		ButtonEventHandler eventHandler = new ButtonEventHandler();
	
		mBackwardBtn.setOnClickListener(eventHandler);
		mForwardBtn.setOnClickListener(eventHandler);
		
		SharedPreferences pref = getSharedPreferences(Constants.GAME_PREFERENCE, Activity.MODE_PRIVATE);
		mCurrentLevel = pref.getInt(Constants.GAME_LEVEL, 0);
		pref = getSharedPreferences(Constants.GAME_RECORD, Activity.MODE_PRIVATE);

		if (mCurrentLevel == 0)
			mCurrentLevel++;
		
		for (int i = 1; i <= Constants.GAME_MAX_LEVEL; i++) {
			mDeathHighScoreList.add(pref.getInt(Constants.GAME_RECORD_PREFIX + i + Constants.GAME_RECORD_POSTFIX_DEATH, -1));
			mTimeHighScoreList.add(pref.getLong(Constants.GAME_RECORD_PREFIX + i + Constants.GAME_RECORD_POSTFIX_TIME, -1));
		}

		Display display = ((WindowManager)this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		mScreenWidth = display.getWidth();
		mScreenHeight = display.getHeight();
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;
		
		Bitmap backgroudImage = BitmapFactory.decodeResource(this.getResources(), R.drawable.background, options);
		backgroudImage = Bitmap.createScaledBitmap(backgroudImage, (int)mScreenWidth, (int)mScreenHeight, true);
		Bitmap leftBackgroundIamge = Bitmap.createBitmap(backgroudImage, 0, 0, backgroudImage.getWidth() / 2, backgroudImage.getHeight());
		Bitmap rightBackgroundIamge = Bitmap.createBitmap(backgroudImage, (int)(mScreenWidth / 2), 0, backgroudImage.getWidth() / 2, backgroudImage.getHeight());
		
		mLeftBackground.setImageBitmap(leftBackgroundIamge);
		mRightBackground.setImageBitmap(rightBackgroundIamge);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if (!mEventFlag) {
			mRecordLayout.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.appear_top_down));
			mLeftBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_open));
			mRightBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_open));

			int deathHighScore = mDeathHighScoreList.get(mCurrentLevel - 1);
			long timeHighScore = mTimeHighScoreList.get(mCurrentLevel - 1);
			
			mRecordTitle.setText(Constants.STAGE_STR + mCurrentLevel + Constants.RECORD_STAGE_STR);
			if (deathHighScore != -1 && timeHighScore != -1) {
				mRecordDeath.setText(deathHighScore + Constants.DEATH_STR);
				mRecordTime.setText(Util.convertTimeToString(timeHighScore));
			} else {
				mRecordDeath.setText(Constants.RECORD_NO_DATA);
				mRecordTime.setText(Constants.RECORD_NO_DATA);
			}
			
			new RecordTask().execute(RecordTask.ACTIVITY_RESUME);
		}
	}
	
	@Override
	protected void onDestroy() {
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mEventFlag) {
			mRecordLayout.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.disappear_top_down));
			mLeftBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_close));
			mRightBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_close));
			
			mEventFlag = false;
			
			mBackwardBtn.setClickable(false);
			mForwardBtn.setClickable(false);
			
			new RecordTask().execute(RecordTask.BACK_KEY);
		}
	}
	
	public void refreshRecord() {
		if (!mEventFlag) {
			mEventFlag = true;
			
			int deathHighScore = mDeathHighScoreList.get(mCurrentLevel - 1);
			long timeHighScore = mTimeHighScoreList.get(mCurrentLevel - 1);
			
			mRecordTitle.setText(Constants.STAGE_STR + mCurrentLevel + Constants.RECORD_STAGE_STR);
			if (deathHighScore != -1 && timeHighScore != -1) {
				mRecordDeath.setText(deathHighScore + Constants.DEATH_STR);
				mRecordTime.setText(Util.convertTimeToString(timeHighScore));
			} else {
				mRecordDeath.setText(Constants.RECORD_NO_DATA);
				mRecordTime.setText(Constants.RECORD_NO_DATA);
			}
		}
	}
	
	private class ButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (mEventFlag) {
				mEventFlag = false;

				switch (v.getId()) {
				case R.id.backwardRecord:
					if (mCurrentLevel > 1) {
						mCurrentLevel--;
						refreshRecord();
					} else {
						mEventFlag = true;
					}
					break;
				case R.id.forwardRecord:
					if (mCurrentLevel < Constants.GAME_MAX_LEVEL) {
						mCurrentLevel++;
						refreshRecord();
					} else {
						mEventFlag = true;
					}
					break;
				}	
			}
		}
	}
	
	private class RecordTask extends AsyncTask<Integer, Void, Void> {
		private static final int WAITING_TIME = 1500;
		
		public static final int ACTIVITY_RESUME = 0;
		
		public static final int BACK_KEY = 2;
		
		@Override
		protected Void doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(WAITING_TIME);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			switch (params[0]) {
			case ACTIVITY_RESUME:
				mEventFlag = true;
				mBackwardBtn.setClickable(true);
				mForwardBtn.setClickable(true);
				break;
			case BACK_KEY:
				finish();
				break;
			}

			return null;
		}
	}
}
