package madcat.studio.watchout.pref;

import madcat.studio.watchout.constants.Constants;
import madcat.studio.watchout.main.R;
import madcat.studio.watchout.utils.Util;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PreferenceActivity extends Activity {
	private Context mContext = null;
	
	private LinearLayout mPrefMenuLayout = null;
	private ImageView mLeftBackground = null;
	private ImageView mRightBackground = null;
	
	private TextView mGameVersionTextView = null;
	private Button mGameInitailizeBtn = null;
	private Button mGameScreenBtn = null;
	private Button mGameVibrationBtn = null;
	
	private int mScreenWidth = 0;
	private int mScreenHeight = 0;
	
	private boolean mEventFlag = false;
	
	private boolean mGameInitializeFlag = false;
	private boolean mGameVirationFlag = true;
	private boolean mGameScreenFlag = true;
	
	private boolean mGameVirationFlagOrigin = true;
	private boolean mGameScreenFlagOrigin = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.preference);

		mContext = this;
		
		SharedPreferences pref = getSharedPreferences(Constants.GAME_PREFERENCE, Activity.MODE_PRIVATE);
		
		mPrefMenuLayout = (LinearLayout)findViewById(R.id.prefMenuLayout);
		mLeftBackground = (ImageView)findViewById(R.id.prefLeftBackground);
		mRightBackground = (ImageView)findViewById(R.id.prefRightBackground);
		
		mGameVersionTextView = (TextView)findViewById(R.id.gameVersion);
		mGameInitailizeBtn = (Button)findViewById(R.id.gameInitialize);
		mGameScreenBtn = (Button)findViewById(R.id.gameScreen);
		mGameVibrationBtn = (Button)findViewById(R.id.gameVibration);
		
		if (pref.getBoolean(Constants.GAME_SCREEN, true)) {
			mGameScreenFlag = mGameScreenFlagOrigin = true;
			mGameScreenBtn.setText(Constants.GAME_SCREEN_ON);
		} else {
			mGameScreenFlag = mGameScreenFlagOrigin = false;
			mGameScreenBtn.setText(Constants.GAME_SCREEN_OFF);
		}
		
		if (pref.getBoolean(Constants.GAME_VIBRATION, true)) {
			mGameVirationFlag = mGameVirationFlagOrigin = true;
			mGameVibrationBtn.setText(Constants.GAME_VIBRATION_ON);
		} else {
			mGameVirationFlag = mGameVirationFlagOrigin = false;
			mGameVibrationBtn.setText(Constants.GAME_VIBRATION_OFF);
		}
			
		Display display = ((WindowManager)this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		mScreenWidth = display.getWidth();
		mScreenHeight = display.getHeight();
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;
		
		Bitmap backgroudImage = BitmapFactory.decodeResource(this.getResources(), R.drawable.background, options);
		backgroudImage = Bitmap.createScaledBitmap(backgroudImage, (int)mScreenWidth, (int)mScreenHeight, true);
		Bitmap leftBackgroundIamge = Bitmap.createBitmap(backgroudImage, 0, 0, backgroudImage.getWidth() / 2, backgroudImage.getHeight());
		Bitmap rightBackgroundIamge = Bitmap.createBitmap(backgroudImage, (int)(mScreenWidth / 2), 0, backgroudImage.getWidth() / 2, backgroudImage.getHeight());
		
		mLeftBackground.setImageBitmap(leftBackgroundIamge);
		mRightBackground.setImageBitmap(rightBackgroundIamge);
		
		ButtonEventHandler buttonEventHandler = new ButtonEventHandler();
		
		mGameVersionTextView.setText(Constants.GAME_VERSION);
		
		mGameInitailizeBtn.setOnClickListener(buttonEventHandler);
		mGameScreenBtn.setOnClickListener(buttonEventHandler);
		mGameVibrationBtn.setOnClickListener(buttonEventHandler);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if (!mEventFlag) {
			mPrefMenuLayout.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.appear_top_down));
			mLeftBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_open));
			mRightBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_open));
			
			new PreferenceMenuTask().execute(PreferenceMenuTask.ACTIVITY_RESUME);
		}
	}
	
	@Override
	protected void onDestroy() {
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mEventFlag) {
			mEventFlag = false;
			
			mGameInitailizeBtn.setClickable(false);
			mGameScreenBtn.setClickable(false);
			mGameVibrationBtn.setClickable(false);
			
			if (mGameInitializeFlag || (mGameScreenFlagOrigin != mGameScreenFlag) || (mGameVirationFlagOrigin != mGameVirationFlag)) {
				new ModifySaveDialog(mContext).show();
			} else {
				mPrefMenuLayout.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.disappear_top_down));
				mLeftBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_close));
				mRightBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_close));
				
				new PreferenceMenuTask().execute(PreferenceMenuTask.BACK_KEY);
			}
		}
	}
	
	private class ButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.gameInitialize:
				new GameInitializeDialog(mContext).show();
				break;
			case R.id.gameScreen:
				if (mGameScreenFlag) {
					mGameScreenFlag = false;
					mGameScreenBtn.setText(Constants.GAME_SCREEN_OFF);
				} else {
					mGameScreenFlag = true;
					mGameScreenBtn.setText(Constants.GAME_SCREEN_ON);
				}
				break;
			case R.id.gameVibration:
				if (mGameVirationFlag) {
					mGameVirationFlag = false;
					mGameVibrationBtn.setText(Constants.GAME_VIBRATION_OFF);
				} else {
					mGameVirationFlag = true;
					mGameVibrationBtn.setText(Constants.GAME_VIBRATION_ON);
				}
				break;
			}
		}
	}
	
	private class PreferenceMenuTask extends AsyncTask<Integer, Void, Void> {
		private static final int WAITING_TIME = 1500;
		
		public static final int ACTIVITY_RESUME = 0;
		
		public static final int BACK_KEY = 2;
		
		@Override
		protected Void doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(WAITING_TIME);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			switch (params[0]) {
			case ACTIVITY_RESUME:
				mEventFlag = true;
				mGameInitailizeBtn.setClickable(true);
				mGameScreenBtn.setClickable(true);
				mGameVibrationBtn.setClickable(true);
				break;
			case BACK_KEY:
				finish();
				break;
			}

			return null;
		}
	}
	
	private class GameInitializeDialog extends Dialog {
		private Button mGameInitializeYesBtn = null;
		private Button mGameInitializeNoBtn = null;
		
		public GameInitializeDialog(Context context) {
			// TODO Auto-generated constructor stub
			super(context);
			
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.pref_initialize_dialog);
			
			mGameInitializeYesBtn = (Button)findViewById(R.id.prefGameInitializeYes);
			mGameInitializeNoBtn = (Button)findViewById(R.id.prefGameInitializeNo);
			
			mGameInitializeYesBtn.setOnClickListener(new android.view.View.OnClickListener() {
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mGameInitializeFlag = true;
					dismiss();
				}
			});
			mGameInitializeNoBtn.setOnClickListener(new android.view.View.OnClickListener() {
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mGameInitializeFlag = false;
					dismiss();
				}
			});
		}
		
		@Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
			dismiss();
		}
	}
	
	private class ModifySaveDialog extends Dialog {
		private Button mPrefModifySaveYesBtn = null;
		private Button mPrefModifySaveNoBtn = null;
		
		public ModifySaveDialog(Context context) {
			// TODO Auto-generated constructor stub
			super(context);
			
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.pref_modify_save_dialog);
			
			mPrefModifySaveYesBtn = (Button)findViewById(R.id.prefModifySaveYes);
			mPrefModifySaveNoBtn = (Button)findViewById(R.id.prefModifySaveNo);
			
			mPrefModifySaveYesBtn.setOnClickListener(new android.view.View.OnClickListener() {
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dismiss();
					
					mPrefMenuLayout.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.disappear_top_down));
					mLeftBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_close));
					mRightBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_close));
						
					SharedPreferences preference = getSharedPreferences(Constants.GAME_PREFERENCE, Activity.MODE_PRIVATE);
					SharedPreferences.Editor preferenceEditor = preference.edit();
					SharedPreferences record = getSharedPreferences(Constants.GAME_RECORD, Activity.MODE_PRIVATE);
					SharedPreferences.Editor recordEditor = record.edit();	
					
					if (mGameInitializeFlag) {
						preferenceEditor.putInt(Constants.GAME_LEVEL, 0);
						
						for (int i = 1; i <= Constants.GAME_MAX_LEVEL; i++) {
							recordEditor.putInt(Constants.GAME_RECORD_PREFIX + i + Constants.GAME_RECORD_POSTFIX_DEATH, -1);
							recordEditor.putLong(Constants.GAME_RECORD_PREFIX + i + Constants.GAME_RECORD_POSTFIX_TIME, -1);
						}
						recordEditor.commit();
					}
						
					preferenceEditor.putBoolean(Constants.GAME_SCREEN, mGameScreenFlag);
					preferenceEditor.putBoolean(Constants.GAME_VIBRATION, mGameVirationFlag);
					preferenceEditor.commit();
					
					new PreferenceMenuTask().execute(PreferenceMenuTask.BACK_KEY);
				}
			});
			mPrefModifySaveNoBtn.setOnClickListener(new android.view.View.OnClickListener() {
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dismiss();
					
					mPrefMenuLayout.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.disappear_top_down));
					mLeftBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_close));
					mRightBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_close));
						
					new PreferenceMenuTask().execute(PreferenceMenuTask.BACK_KEY);
				}
			});
		}
		
		@Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
			dismiss();
			
			mPrefMenuLayout.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.disappear_top_down));
			mLeftBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_close));
			mRightBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_close));
				
			new PreferenceMenuTask().execute(PreferenceMenuTask.BACK_KEY);
		}
	}
}