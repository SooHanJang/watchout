package madcat.studio.watchout.gameBlock;

import java.io.Serializable;

import madcat.studio.watchout.game.GameBuilder;
import android.graphics.RectF;

public class GameAttackerBlock extends RectF implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -148760024629148130L;
	
	private int mScreenWidth = 0;
	private int mScreenHeight = 0;
	
	private int mBlockStartOffset = 0;
	private int mBlockWidth = 0;
	private int mBlockHeight = 0;
	
	private int mBlockPosition = 0;
	
	private int mSpeed = 0;
	private int mDirection = 0;
	private int mPattern = 0;
	private int mRepeat = 0;
	
	private int mRepeatStartPoint = 0;
	private int mRepeatEndPoint = 0;
	
	private int mInitLeft = 0;
	private int mInitRight = 0;
	private int mInitTop = 0;
	private int mInitBottom = 0;

	private int mInitDirection = 0;
	
	public GameAttackerBlock(int screenWidth, int screenHeight, float blockStartOffset, float blockPosition, float blockWidth, float speed, int direction, int pattern, int movingRange) {
		// TODO Auto-generated constructor stub
		mScreenWidth = screenWidth;
		mScreenHeight = screenHeight;
		
		mBlockStartOffset = (int)(blockStartOffset * mScreenWidth);
		mBlockWidth = (int)(blockWidth * mScreenWidth);
		mBlockHeight = (int)(mScreenHeight / 20);
		mBlockPosition = (int)(blockPosition * mScreenHeight);

		mSpeed = (int)(speed * mScreenWidth);
		mDirection = mInitDirection = direction;
		mPattern = pattern;
		mRepeat = movingRange;
		
		switch (mDirection) {
		case GameBuilder.BLOCK_DIRECTION_LR:
			left = mBlockStartOffset;
			top = mInitTop = mBlockPosition;
			right = left + mBlockWidth;
			bottom = mInitBottom = (int)(top + mBlockHeight);
			
			mInitLeft = 0;
			mInitRight = mBlockWidth;
			break;
		case GameBuilder.BLOCK_DIRECTION_RL:
			left = mScreenWidth - mBlockWidth - mBlockStartOffset;
			top = mInitTop = mBlockPosition;
			right = mScreenWidth - mBlockStartOffset;
			bottom = mInitBottom = (int)(top + mBlockHeight);
			
			mInitLeft = mScreenWidth - mBlockWidth;
			mInitRight = mScreenWidth;
			break;
		}

		switch (mRepeat) {
		case GameBuilder.BLOCK_REPEAT_FULL:
			mRepeatStartPoint = 0;
			mRepeatEndPoint = mScreenWidth;
			break;
		case GameBuilder.BLOCK_REPEAT_HALF:
			switch (mDirection) {
			case GameBuilder.BLOCK_DIRECTION_LR:
				mRepeatStartPoint = 0;
				mRepeatEndPoint = mScreenWidth / 9 * 4;
				break;
			case GameBuilder.BLOCK_DIRECTION_RL:
				mRepeatStartPoint = mScreenWidth / 9 * 5;
				mRepeatEndPoint = mScreenWidth;
				break;
			}
			break;
		}
	}
	
	public void restart() {
		mDirection = mInitDirection;
		
		switch (mDirection) {
		case GameBuilder.BLOCK_DIRECTION_LR:
			left = mBlockStartOffset;
			top = mInitTop = mBlockPosition;
			right = left + mBlockWidth;
			bottom = mInitBottom = (int)(top + mBlockHeight);
			
			mInitLeft = 0;
			mInitRight = mBlockWidth;
			break;
		case GameBuilder.BLOCK_DIRECTION_RL:
			left = mScreenWidth - mBlockWidth - mBlockStartOffset;
			top = mInitTop = mBlockPosition;
			right = mScreenWidth - mBlockStartOffset;
			bottom = mInitBottom = (int)(top + mBlockHeight);
			
			mInitLeft = mScreenWidth - mBlockWidth;
			mInitRight = mScreenWidth;
			break;
		}
		
		switch (mRepeat) {
		case GameBuilder.BLOCK_REPEAT_FULL:
			mRepeatStartPoint = 0;
			mRepeatEndPoint = mScreenWidth;
			break;
		case GameBuilder.BLOCK_REPEAT_HALF:
			switch (mDirection) {
			case GameBuilder.BLOCK_DIRECTION_LR:
				mRepeatStartPoint = 0;
				mRepeatEndPoint = mScreenWidth / 9 * 4;
				break;
			case GameBuilder.BLOCK_DIRECTION_RL:
				mRepeatStartPoint = mScreenWidth / 9 * 5;
				mRepeatEndPoint = mScreenWidth;
				break;
			}
			break;
		}
	}
	
	private void initialize() {
		left = mInitLeft;
		top = mInitTop;
		right = mInitRight;
		bottom = mInitBottom;
		
		mDirection = mInitDirection;
	}
	
	public void move() {
		switch (mDirection) {
		case GameBuilder.BLOCK_DIRECTION_LR:
			switch (mPattern) {
			case GameBuilder.BLOCK_PATTERN_RESTART:
				left += mSpeed;
				right += mSpeed;
				
				if (right >= mRepeatEndPoint)
					initialize();
				break;
			case GameBuilder.BLOCK_PATTERN_REVERSE:
				left += mSpeed;
				right += mSpeed;
				
				if (right >= mRepeatEndPoint)
					mDirection = GameBuilder.BLOCK_DIRECTION_RL;
				break;
			}
			break;
		case GameBuilder.BLOCK_DIRECTION_RL:
			switch (mPattern) {
			case GameBuilder.BLOCK_PATTERN_RESTART:
				left -= mSpeed;
				right -= mSpeed;
				
				if (left <= mRepeatStartPoint)
					initialize();
				break;
			case GameBuilder.BLOCK_PATTERN_REVERSE:
				left -= mSpeed;
				right -= mSpeed;
				
				if (left <= mRepeatStartPoint)
					mDirection = GameBuilder.BLOCK_DIRECTION_LR;
				break;
			}
			break;
		}
	}
}
