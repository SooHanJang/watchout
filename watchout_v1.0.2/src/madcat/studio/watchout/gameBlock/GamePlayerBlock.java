package madcat.studio.watchout.gameBlock;

import java.io.Serializable;

import android.graphics.RectF;

public class GamePlayerBlock extends RectF implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6580467110265834757L;

	private float mScreenWidth = 0;
	private float mScreenHeight = 0;
	
	private float mBlockWidth = 0;
	private float mBlockHeight = 0;
	
	private float mPrePoint = 0;
	private float mPostPoint = 0;
	
	public GamePlayerBlock(float screenWidth, float screenHeight, float blockWidth) {
		// TODO Auto-generated constructor stub
		mScreenWidth = screenWidth;
		mScreenHeight = screenHeight;
		mBlockWidth = blockWidth * mScreenWidth;
		mBlockHeight = (float)(screenHeight / 20);
		
		left = mScreenWidth / 2 - mBlockWidth / 2;
		top = 0;
		right = mScreenWidth / 2 + mBlockWidth / 2;
		bottom = top + mBlockHeight;
	}
	
	public void initialize() {
		left = mScreenWidth / 2 - mBlockWidth / 2;
		top = 0;
		right = mScreenWidth / 2 + mBlockWidth / 2;
		bottom = top + mBlockHeight;
		
		mPrePoint = 0;
		mPostPoint = 0;
	}
	
	public void setPoint(float point) {
		if (mPrePoint == 0 && mPostPoint == 0) {
			mPrePoint = point;
			mPostPoint = point;
		} else {
			if (Math.abs(point - mPrePoint) < mScreenHeight / 10) {
				if (point - mPrePoint > 0) {
					if (bottom + (point - mPrePoint) <= mScreenHeight) {

						mPrePoint = mPostPoint;
						mPostPoint = point;
							
						top += (point - mPrePoint);
						bottom += (point - mPrePoint);
					}
				} else if (point - mPrePoint < 0) {
					if (top + (point - mPrePoint) >= 0) {
						mPrePoint = mPostPoint;
						mPostPoint = point;
							
						top += (point - mPrePoint);
						bottom += (point - mPrePoint);
					}
				}
			}
		}
	}
}