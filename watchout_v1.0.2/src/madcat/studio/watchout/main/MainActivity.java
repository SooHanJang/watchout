package madcat.studio.watchout.main;

import madcat.studio.watchout.constants.Constants;
import madcat.studio.watchout.game.GameActivity;
import madcat.studio.watchout.game.GameBuilder;
import madcat.studio.watchout.pref.PreferenceActivity;
import madcat.studio.watchout.record.RecordActivity;
import madcat.studio.watchout.stage.StageActivity;
import madcat.studio.watchout.utils.Util;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainActivity extends Activity {
	private Context mContext = null;
	
	private LinearLayout mMainMenuLayout = null;
	private ImageView mLeftBackground = null;
	private ImageView mRightBackground = null;
	
	private Button mQuickStartGameBtn = null;
	private Button mStartGameBtn = null;
	private Button mRecordBtn = null;
	private Button mPreferenceBtn = null;
	
	private int mScreenWidth = 0;
	private int mScreenHeight = 0;
	
	private boolean mEventFlag = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.main);

		mContext = this;
		
		mMainMenuLayout = (LinearLayout)findViewById(R.id.mainMenuLayout);
		mLeftBackground = (ImageView)findViewById(R.id.mainLeftBackground);
		mRightBackground = (ImageView)findViewById(R.id.mainRightBackground);
		
		mQuickStartGameBtn = (Button)findViewById(R.id.quickStartGame);
		mStartGameBtn = (Button)findViewById(R.id.startGame);
		mRecordBtn = (Button)findViewById(R.id.gameRecord);
		mPreferenceBtn = (Button)findViewById(R.id.preference);
		
		Display display = ((WindowManager)this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		mScreenWidth = display.getWidth();
		mScreenHeight = display.getHeight();
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;
		
		Bitmap backgroudImage = BitmapFactory.decodeResource(this.getResources(), R.drawable.background, options);
		backgroudImage = Bitmap.createScaledBitmap(backgroudImage, (int)mScreenWidth, (int)mScreenHeight, true);
		Bitmap leftBackgroundIamge = Bitmap.createBitmap(backgroudImage, 0, 0, backgroudImage.getWidth() / 2, backgroudImage.getHeight());
		Bitmap rightBackgroundIamge = Bitmap.createBitmap(backgroudImage, (int)(mScreenWidth / 2), 0, backgroudImage.getWidth() / 2, backgroudImage.getHeight());
		
		mLeftBackground.setImageBitmap(leftBackgroundIamge);
		mRightBackground.setImageBitmap(rightBackgroundIamge);
		
		ButtonEventHandler buttonEventHandler = new ButtonEventHandler();
		
		mQuickStartGameBtn.setOnClickListener(buttonEventHandler);
		mStartGameBtn.setOnClickListener(buttonEventHandler);
		mRecordBtn.setOnClickListener(buttonEventHandler);
		mPreferenceBtn.setOnClickListener(buttonEventHandler);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if (!mEventFlag) {
			mMainMenuLayout.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.appear_top_down));
			mLeftBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_open));
			mRightBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_open));

			new MainMenuTask().execute(MainMenuTask.ACTIVITY_RESUME);
		}
	}
	
	@Override
	protected void onDestroy() {
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mEventFlag) {
			mEventFlag = false;
			
			mQuickStartGameBtn.setClickable(false);
			mStartGameBtn.setClickable(false);
			mPreferenceBtn.setClickable(false);
			
			mMainMenuLayout.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.disappear_top_down));
			mLeftBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_close));
			mRightBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_close));
			
			new MainMenuTask().execute(MainMenuTask.EXIT_APPLICATION);
		}
	}
	
	private class ButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (mEventFlag) {
				mEventFlag = false;
				
				mLeftBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_close));
				mRightBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_close));
				mMainMenuLayout.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.disappear_top_down));

				mQuickStartGameBtn.setClickable(false);
				mStartGameBtn.setClickable(false);
				mPreferenceBtn.setClickable(false);
				
				switch (v.getId()) {
				case R.id.quickStartGame:
					new MainMenuTask().execute(MainMenuTask.QUICK_START_GAME);
					break;
				case R.id.startGame:
					new MainMenuTask().execute(MainMenuTask.START_GAME);
					break;
				case R.id.gameRecord:
					new MainMenuTask().execute(MainMenuTask.GAME_RECORD);
					break;
				case R.id.preference:
					new MainMenuTask().execute(MainMenuTask.EDIT_PREFERENCE);
					break;
				}
			}
		}
	}
	
	private class MainMenuTask extends AsyncTask<Integer, Void, Void> {
		private static final int WAITING_TIME = 1500;
		
		public static final int ACTIVITY_RESUME = 0;
		
		public static final int QUICK_START_GAME = 2;
		public static final int START_GAME = 3;
		public static final int GAME_RECORD = 4;
		public static final int EDIT_PREFERENCE = 5;
		public static final int EXIT_APPLICATION = 6;
		
		@Override
		protected Void doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(WAITING_TIME);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Intent intent = null;
			
			switch (params[0]) {
			case ACTIVITY_RESUME:
				mEventFlag = true;
				mQuickStartGameBtn.setClickable(true);
				mStartGameBtn.setClickable(true);
				mPreferenceBtn.setClickable(true);
				break;
			case QUICK_START_GAME:
				SharedPreferences pref = getSharedPreferences(Constants.GAME_PREFERENCE, Activity.MODE_PRIVATE);
				int currentGameLevel = pref.getInt(Constants.GAME_LEVEL, 0) + 1;
				
				intent = new Intent(MainActivity.this, GameActivity.class);
				intent.putExtra(GameActivity.CALL_POINT, GameActivity.CALL_POINT_MAIN);
				intent.putExtra(GameBuilder.STAGE_NUMBER, currentGameLevel);
				break;
			case START_GAME:
				intent = new Intent(MainActivity.this, StageActivity.class);
				break;
			case GAME_RECORD:
				intent = new Intent(MainActivity.this, RecordActivity.class);
				break;
			case EDIT_PREFERENCE:
				intent = new Intent(MainActivity.this, PreferenceActivity.class);
				break;
			case EXIT_APPLICATION:
				finish();
				break;
			}
			
			if (intent != null)
				startActivity(intent);
			
			return null;
		}
	}
}
