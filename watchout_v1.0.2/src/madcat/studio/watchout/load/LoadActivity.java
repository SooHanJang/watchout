package madcat.studio.watchout.load;

import madcat.studio.watchout.main.MainActivity;
import madcat.studio.watchout.main.R;
import madcat.studio.watchout.utils.Util;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class LoadActivity extends Activity {
	private LoaddingTask mLoaddingTask = null;

	private boolean mEventFlag = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.load);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if (mEventFlag) {
			mEventFlag = false;
			
			mLoaddingTask = new LoaddingTask();
			mLoaddingTask.execute();
		}
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
		if (mEventFlag) {
			mEventFlag = false;
			
			if (mLoaddingTask != null) {
				mLoaddingTask.cancel(true);
			}
			finish();
		}
	}
	
	@Override
	protected void onDestroy() {
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mEventFlag) {
			mEventFlag = false;
			
			if (mLoaddingTask != null) {
				mLoaddingTask.cancel(true);
			}
			finish();
		}
	}
	
	private class LoaddingTask extends AsyncTask<Void, Void, Void> {
		private static final int WAITING_TIME = 1500;
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(WAITING_TIME);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Intent intent = new Intent(LoadActivity.this, MainActivity.class);
			startActivity(intent);

			finish();
			
			return null;
		}
	}
}
