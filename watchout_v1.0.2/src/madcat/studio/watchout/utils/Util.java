package madcat.studio.watchout.utils;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

/**
 * 
 * @author Dane
 * !!DO NOT CHANGE THIS FILE!!
 */
public class Util {
	/**
	 * long 형태의 time을 받아, String 형태로 변환하는 함수.
	 * @param time
	 * @return timeStr [String으로 변환된 Time]
	 */
	public static String convertTimeToString(long time) {
		String timeStr = "";
		
		long ms = time % 1000;
		long s = ((time - ms) / 1000) % 60;
		long m = (((time - ms) / 1000) - s) / 60;
		
		if (m >= 10)
			timeStr += m + ":";
		else
			timeStr += "0" + m + ":";
		
		if (s >= 10)
			timeStr += s + ".";
		else
			timeStr += "0" + s + ".";
		
		if (ms >= 10)
			if (ms >= 100)
				timeStr += ms;
			else
				timeStr += "0" + ms;
		else
			timeStr += "00" + ms;
		
		return timeStr;
	}
	
	/**
	 * 부모 View를 기준으로 자식 View를 재귀 호출하면서 할당된 이미지를 해제한다.
	 * @param root
	 */
	
	public static void recursiveRecycle(View root) {
		if(root == null) {
			return;
		}
		
		if(root instanceof ViewGroup) {
			ViewGroup group = (ViewGroup)root;
			int count = group.getChildCount();
			
			for(int i=0; i < count; i++) {
				recursiveRecycle(group.getChildAt(i));
			}
			
			if(!(root instanceof AdapterView)) {
				group.removeAllViews();
			}
		}
		
		if(root instanceof ImageView) {
			((ImageView)root).setImageDrawable(null);
		}
		
		root.setBackgroundDrawable(null);
		
		root = null;
		
		return;
	}
}
