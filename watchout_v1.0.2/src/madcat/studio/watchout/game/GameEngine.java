package madcat.studio.watchout.game;

import java.util.ArrayList;

import madcat.studio.watchout.constants.Constants;
import madcat.studio.watchout.gameBlock.GameAttackerBlock;
import madcat.studio.watchout.gameBlock.GamePlayerBlock;
import madcat.studio.watchout.main.R;
import madcat.studio.watchout.utils.Util;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.os.Vibrator;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.WindowManager;

public class GameEngine extends Thread {
	public static final int PLAYER_IDLE = 0;
	public static final int PLAYER_MOVING = 1;
	
	private Context mContext = null;
	private SurfaceHolder mSurfaceHolder = null;
	
	private float mScreenWidth, mScreenHeight;
	
	private Bitmap mLeftBackgroundImage = null;
	private Bitmap mRightBackgroundImage = null;
	
	private GameBuilder mGameBuilder = null;
	private GamePlayerBlock mGamePlayerBlock = null;
	private ArrayList<GameAttackerBlock> mGameAttackerBlockList = null;
	
	private int mState = PLAYER_IDLE;
	
	private boolean mEnterFlag = false;
	private boolean mRunFlag = false;
	private boolean mWaitFlag = true;

	private boolean mGameVibrationFlag = true;
	
	private long mStartTime = 0;
	private long mCurrentTime = 0;
	
	private int mDeathNumber = 0;
	
	private int mDeathHighScore = 0;
	private long mTimeHighScore = 0;
	
	public GameEngine(Context context, SurfaceHolder surfaceHolder, GameBuilder gameBuilder) {
		// TODO Auto-generated constructor stub
		mContext = context;
		mSurfaceHolder = surfaceHolder;
		mGameBuilder = gameBuilder;
		
		Display display = ((WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		mScreenWidth = display.getWidth();
		mScreenHeight = display.getHeight();
	
		Bitmap backgroudImage = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.background);
		backgroudImage = Bitmap.createScaledBitmap(backgroudImage, (int)mScreenWidth, (int)mScreenHeight, true);
		mLeftBackgroundImage = Bitmap.createBitmap(backgroudImage, 0, 0, backgroudImage.getWidth() / 2, backgroudImage.getHeight());
		mRightBackgroundImage = Bitmap.createBitmap(backgroudImage, (int)(mScreenWidth / 2), 0, backgroudImage.getWidth() / 2, backgroudImage.getHeight());
		
		mGameBuilder.buildGame();
		mGamePlayerBlock = gameBuilder.getGamePlayerBlock();
		mGameAttackerBlockList = gameBuilder.getGameAttackerBlockList();

		SharedPreferences pref = mContext.getSharedPreferences(Constants.GAME_PREFERENCE, Activity.MODE_PRIVATE);
		mGameVibrationFlag = pref.getBoolean(Constants.GAME_VIBRATION, true);
		
		pref = mContext.getSharedPreferences(Constants.GAME_RECORD, Activity.MODE_PRIVATE);
		mDeathHighScore = pref.getInt(Constants.GAME_RECORD_PREFIX + mGameBuilder.getStageNumber() + Constants.GAME_RECORD_POSTFIX_DEATH, -1);
		mTimeHighScore = pref.getLong(Constants.GAME_RECORD_PREFIX + mGameBuilder.getStageNumber() + Constants.GAME_RECORD_POSTFIX_TIME, -1);
	}	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Paint paint = new Paint();
		Canvas canvas = null;
		mRunFlag = true;
		mWaitFlag = false;
		
		paint.setAntiAlias(true);
		
		for (float i = 0; i <= (float)(mScreenWidth / 6); i += i * (float)0.1 + (float)1.0) {
			canvas = mSurfaceHolder.lockCanvas();
			try {
				synchronized (mSurfaceHolder) {
					paint.setColor(Color.WHITE);
					canvas.drawRect(0, 0, mScreenWidth, mScreenHeight, paint);
					canvas.drawBitmap(mLeftBackgroundImage, -i, 0, paint);
					canvas.drawBitmap(mRightBackgroundImage, mScreenWidth / 2 + i, 0, paint);
				}
			} finally {
				if (canvas != null) {
					mSurfaceHolder.unlockCanvasAndPost(canvas);
				}
			}
		}
		
		for (int i = 255; i > 0; i -= 20) {
			canvas = mSurfaceHolder.lockCanvas();
			try {
				synchronized (mSurfaceHolder) {
					paint.setAlpha(255);
					paint.setColor(Color.WHITE);
					canvas.drawRect(0, 0, mScreenWidth, mScreenHeight, paint);
					
					paint.setColor(Color.BLACK);
					canvas.drawRect(0, 0, mScreenWidth / 3, mScreenHeight, paint);
					canvas.drawRect(mScreenWidth / 3 * 2, 0, mScreenWidth, mScreenHeight, paint);
					
					paint.setAlpha(i);
					canvas.drawBitmap(mLeftBackgroundImage, mScreenWidth / 6 * -1, 0, paint);
					canvas.drawBitmap(mRightBackgroundImage, mScreenWidth / 2 + mScreenWidth / 6, 0, paint);
				}
			} finally {
				if (canvas != null) {
					mSurfaceHolder.unlockCanvasAndPost(canvas);
				}
			}
		}
		
		mEnterFlag = true;
		
		mStartTime = System.currentTimeMillis();
		mCurrentTime = System.currentTimeMillis();
		
		while (mRunFlag) {
			while (!mWaitFlag) {
				canvas = mSurfaceHolder.lockCanvas();
				
				try {
					synchronized (mSurfaceHolder) {
						mCurrentTime = System.currentTimeMillis();
						
						paint.setColor(Color.WHITE);
						canvas.drawRect(0, 0, mScreenWidth, mScreenHeight, paint);
						
						paint.setColor(Color.LTGRAY);
						canvas.drawRect(mScreenWidth / 3, mScreenHeight - mScreenHeight / 20, mScreenWidth / 3 * 2, mScreenHeight, paint);
						paint.setColor(Color.WHITE);
						paint.setTextSize(20);
						paint.setTextAlign(Align.CENTER);
						canvas.drawText(Constants.GOAL_STR, mScreenWidth / 2, mScreenHeight - mScreenHeight / 80, paint);
						
						if (isDead()) {
							mState = PLAYER_IDLE;
							mGamePlayerBlock.initialize();
							for (int i = 0; i < mGameAttackerBlockList.size(); i++)
								mGameAttackerBlockList.get(i).restart();
						} else {
							for (int i = 0; i < mGameAttackerBlockList.size(); i++)
								mGameAttackerBlockList.get(i).move();
							
							drawAttacker(canvas);
							drawPlayer(canvas);
						}
						
						paint.setColor(Color.BLACK);
						canvas.drawRect(0, 0, mScreenWidth / 3, mScreenHeight, paint);
						canvas.drawRect(mScreenWidth / 3 * 2, 0, mScreenWidth, mScreenHeight, paint);
						
						paint.setColor(Color.WHITE);
						paint.setTextSize(20);
						paint.setTextAlign(Align.CENTER);
						canvas.drawText(Constants.STAGE_STR + mGameBuilder.getStageNumber(), mScreenWidth / 6, mScreenHeight / 30, paint);
						
						canvas.drawText(Constants.CURRENTSCORE_STR, mScreenWidth / 6 * 5, mScreenHeight / 30, paint);
						canvas.drawText(mDeathNumber + Constants.DEATH_STR, mScreenWidth / 6 * 5, mScreenHeight / 30 * 2, paint);
						canvas.drawText(Util.convertTimeToString(mCurrentTime - mStartTime), mScreenWidth / 6 * 5, mScreenHeight / 30 * 3, paint);
						
						canvas.drawText(Constants.HIGHSCORE_STR, mScreenWidth / 6 * 5, mScreenHeight / 30 * 5, paint);
						if (mDeathHighScore == -1 && mTimeHighScore == -1) {
							canvas.drawText(Constants.RECORD_NO_DATA, mScreenWidth / 6 * 5, mScreenHeight / 30 * 6, paint);
						} else {
							canvas.drawText(mDeathHighScore + Constants.DEATH_STR, mScreenWidth / 6 * 5, mScreenHeight / 30 * 6, paint);
							canvas.drawText(Util.convertTimeToString(mTimeHighScore), mScreenWidth / 6 * 5, mScreenHeight / 30 * 7, paint);
						}
					}
				} finally {
					if (canvas != null)
						mSurfaceHolder.unlockCanvasAndPost(canvas);
				}
				
				if (isClear())
					gameClear();
				
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void engineResume() {
		if (mWaitFlag) {
			mWaitFlag = false;
			synchronized (this) {
				notify();
			}
		}
	}
	
	public void enginePause() {
		if (!mWaitFlag) {
			mWaitFlag = true;
			synchronized (this) {
				notify();
			}
		}
	}
	
	public void engineStop() {
		if (mRunFlag) {
			mRunFlag = false;
			mWaitFlag = true;
			synchronized (this) {
				notify();
			}
			
			Paint paint = new Paint();
			Canvas canvas = null;
			
			paint.setAntiAlias(true);
			
			for (float i = 0; i <= (float)(mScreenWidth / 2); i += i * (float)0.04 + (float)1.0) {
				canvas = mSurfaceHolder.lockCanvas();
				try {
					synchronized (mSurfaceHolder) {
						paint.setColor(Color.WHITE);
						canvas.drawRect(0, 0, mScreenWidth, mScreenHeight, paint);
						
						paint.setColor(Color.LTGRAY);
						canvas.drawRect(mScreenWidth / 3, mScreenHeight - mScreenHeight / 20, mScreenWidth / 3 * 2, mScreenHeight, paint);
						paint.setColor(Color.WHITE);
						paint.setTextSize(20);
						paint.setTextAlign(Align.CENTER);
						canvas.drawText(Constants.GOAL_STR, mScreenWidth / 2, mScreenHeight - mScreenHeight / 80, paint);
						
						drawAttacker(canvas);
						drawPlayer(canvas);
						
						paint.setColor(Color.BLACK);
						canvas.drawRect(0, 0, mScreenWidth / 3, mScreenHeight, paint);
						canvas.drawRect(mScreenWidth / 3 * 2, 0, mScreenWidth, mScreenHeight, paint);
						
						paint.setColor(Color.WHITE);
						paint.setTextSize(20);
						paint.setTextAlign(Align.CENTER);
						canvas.drawText(Constants.STAGE_STR + mGameBuilder.getStageNumber(), mScreenWidth / 6, mScreenHeight / 30, paint);
					
						canvas.drawText(Constants.CURRENTSCORE_STR, mScreenWidth / 6 * 5, mScreenHeight / 30, paint);
						canvas.drawText(mDeathNumber + Constants.DEATH_STR, mScreenWidth / 6 * 5, mScreenHeight / 30 * 2, paint);
						canvas.drawText(Util.convertTimeToString(mCurrentTime - mStartTime), mScreenWidth / 6 * 5, mScreenHeight / 30 * 3, paint);
						
						canvas.drawText(Constants.HIGHSCORE_STR, mScreenWidth / 6 * 5, mScreenHeight / 30 * 5, paint);
						if (mDeathHighScore == -1 && mTimeHighScore == -1) {
							canvas.drawText(Constants.RECORD_NO_DATA, mScreenWidth / 6 * 5, mScreenHeight / 30 * 6, paint);
						} else {
							canvas.drawText(mDeathHighScore + Constants.DEATH_STR, mScreenWidth / 6 * 5, mScreenHeight / 30 * 6, paint);
							canvas.drawText(Util.convertTimeToString(mTimeHighScore), mScreenWidth / 6 * 5, mScreenHeight / 30 * 7, paint);
						}
						
						canvas.drawBitmap(mLeftBackgroundImage, mScreenWidth / 2 * -1 + i, 0, paint);
						canvas.drawBitmap(mRightBackgroundImage, mScreenWidth - i, 0, paint);
					}
				} finally {
					if (canvas != null) {
						mSurfaceHolder.unlockCanvasAndPost(canvas);
					}
				}
			}
			
			canvas = mSurfaceHolder.lockCanvas();
			try {
				synchronized (mSurfaceHolder) {
					paint.setColor(Color.WHITE);
					canvas.drawRect(0, 0, mScreenWidth, mScreenHeight, paint);
					canvas.drawBitmap(mLeftBackgroundImage, 0, 0, paint);
					canvas.drawBitmap(mRightBackgroundImage, mScreenWidth / 2, 0, paint);
				}
			} finally {
				if (canvas != null) {
					mSurfaceHolder.unlockCanvasAndPost(canvas);
				}
			}
		}
	}

	public boolean isEnterAnimationFinish() {
		if (mEnterFlag)
			return true;
		
		return false;
	}
	
	private boolean isDead() {
		for (GameAttackerBlock attackerBlock : mGameAttackerBlockList) {
			if (mGamePlayerBlock.contains(attackerBlock.right, attackerBlock.top) ||
				mGamePlayerBlock.contains(attackerBlock.right, attackerBlock.bottom) ||
				mGamePlayerBlock.contains(attackerBlock.left, attackerBlock.top) ||
				mGamePlayerBlock.contains(attackerBlock.left, attackerBlock.bottom)) {
				
				if (mGameVibrationFlag) {
					Vibrator vibrator = (Vibrator)mContext.getSystemService(Context.VIBRATOR_SERVICE);
					vibrator.vibrate(100);
				}
				
				mDeathNumber++;
				return true;
			}
		}

		return false;
	}
	
	private boolean isClear() {
		if (mGamePlayerBlock.contains(mScreenWidth / 2, mScreenHeight - mScreenHeight / 80 * 3)) {
			if (mGameVibrationFlag) {
				Vibrator vibrator = (Vibrator)mContext.getSystemService(Context.VIBRATOR_SERVICE);
				vibrator.vibrate(100);
			}
			return true;
		}

		return false;
	}
	
	private void gameClear() {
		Intent intent = new Intent(Constants.BROADCAST_STAGE_ACTIVITY);
		intent.putExtra(GameActivity.CLEAR_STAGE, mGameBuilder.getStageNumber());
		intent.putExtra(GameActivity.CLEAR_TIME, mCurrentTime - mStartTime);
		intent.putExtra(GameActivity.CLEAR_DEATH, mDeathNumber);
		mContext.sendBroadcast(intent);
	}
	
	//-----------------------------------
	//	PLAYER METHODS BEGIN
	//-----------------------------------
	public boolean isTouchEventOnPlayerBlock(float y) {
		if (mGamePlayerBlock.contains(mScreenWidth / 2, y))
			return true;
		else
			return false;
	}
	
	public void setPlayerState(int STATE) {
		mState = STATE;
	}
	
	public int getPlayerState() {
		return mState;
	}
	
	public void setPlayerMove(float point) {
		mGamePlayerBlock.setPoint(point);
	}
	
	private void drawPlayer(Canvas canvas) {
		Paint paint = new Paint();
		paint.setColor(Color.GREEN);
		
		canvas.drawRect(mGamePlayerBlock, paint);
	}
	//-----------------------------------
	//	PLAYER METHODS END
	//-----------------------------------
	
	//-----------------------------------
	//	ATTACKER METHODS BEGIN
	//-----------------------------------
	private void drawAttacker(Canvas canvas) {
		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		
		for (GameAttackerBlock attackerBlock : mGameAttackerBlockList) {
			canvas.drawRect(attackerBlock, paint);
		}
	}
	//-----------------------------------
	//	ATTACKER METHODS END
	//-----------------------------------
}