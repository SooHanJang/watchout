package madcat.studio.watchout.game;

import android.content.Context;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

public class GameSurfaceView extends SurfaceView implements Callback {
	private Context mContext = null;
	private SurfaceHolder mSurfaceHolder = null;
	private GameBuilder mGameBuilder = null;
	private GameEngine mGameEngine = null;
	
	public GameSurfaceView(Context context, GameBuilder gameBuilder) {
		// TODO Auto-generated constructor stub
		super(context);

		mContext = context;
		mSurfaceHolder = getHolder();
		mSurfaceHolder.addCallback(this);

		mGameBuilder = gameBuilder;
		mGameEngine = new GameEngine(mContext, mSurfaceHolder, mGameBuilder);
		
		setFocusable(true);
	}

	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		if (mGameEngine != null) {
			try {
				mGameEngine.start();
			} catch (Exception e) {
				restartGameEngine();
			}
		}
	}
	
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		if (mGameEngine != null) {
			stopGameEngine();
		}
	}
	
	public void resumeGameEngine() {
		mGameEngine.engineResume();
	}

	public void pauseGameEngine() {
		mGameEngine.enginePause();
	}
	
	public void stopGameEngine() {
		mGameEngine.engineStop();
	}
	
	public void restartGameEngine() {
		mGameEngine.engineStop();
		
		mGameEngine = null;
		mGameEngine = new GameEngine(mContext, mSurfaceHolder, mGameBuilder);
		mGameEngine.start();
	}
	
	public boolean isEnterAnimationFinish() {
		if (mGameEngine.isEnterAnimationFinish())
			return true;
		
		return false;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		super.onTouchEvent(event);
		
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			if (mGameEngine.isTouchEventOnPlayerBlock(event.getY()))
				if (mGameEngine.getPlayerState() == GameEngine.PLAYER_IDLE)
					mGameEngine.setPlayerState(GameEngine.PLAYER_MOVING);
			break;
		case MotionEvent.ACTION_MOVE:
			if (mGameEngine.getPlayerState() == GameEngine.PLAYER_MOVING)
				mGameEngine.setPlayerMove(event.getY());
			break;
		case MotionEvent.ACTION_UP:
			if (mGameEngine.getPlayerState() == GameEngine.PLAYER_MOVING) {
				mGameEngine.setPlayerState(GameEngine.PLAYER_IDLE);
				mGameEngine.setPlayerMove(event.getY());
			}
			break;
		}
		
		return true;
	}
}
