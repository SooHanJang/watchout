package madcat.studio.watchout.game;

import madcat.studio.watchout.constants.Constants;
import madcat.studio.watchout.main.R;
import madcat.studio.watchout.stage.StageActivity;
import madcat.studio.watchout.utils.Util;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class GameActivity extends Activity {
	public static final String CALL_POINT = "CALL_POINT";
	public static final int CALL_POINT_STAGE = 0;
	public static final int CALL_POINT_MAIN = 1;
	public static final int CALL_POINT_STAGE_GAME = 2;
	public static final int CALL_POINT_MAIN_GAME = 3;
	
	public static final String CLEAR_STAGE = "CLEAR_STAGE";
	public static final String CLEAR_TIME = "CLEAR_TIME";
	public static final String CLEAR_DEATH = "CLEAR_DEATH";
	
	private static final String WAKE_ALWAYS = "hard_WakeAlways";
	
	private Context mContext = null;
	
	private GameBuilder mGameBuilder = null;
	private GameSurfaceView mGameSurfaceView = null;
	
	private int mCallPoint = CALL_POINT_MAIN;
	
	private int mScreenWidth = 0;
	private int mScreenHeight = 0;
	
	private boolean mEventFlag = false;
	private boolean mGameScreenFlag = true;

	private WakeLock mWakeLock = null;
	
	private BroadcastReceiver mBroadCastReceiverForActivityFinish = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			mGameSurfaceView.stopGameEngine();
			
			int clearStage = intent.getIntExtra(CLEAR_STAGE, 0);
			long clearTime = intent.getLongExtra(CLEAR_TIME, 99999999);
			int clearDead = intent.getIntExtra(CLEAR_DEATH, 99999999);
			
			SharedPreferences pref = getSharedPreferences(Constants.GAME_PREFERENCE, Activity.MODE_PRIVATE);
			SharedPreferences.Editor prefEditor = pref.edit();
			
			if (pref.getInt(Constants.GAME_LEVEL, 0) < clearStage) {
				prefEditor.putInt(Constants.GAME_LEVEL, clearStage);
				prefEditor.commit();
			}
			
			pref = getSharedPreferences(Constants.GAME_RECORD, Activity.MODE_PRIVATE);
			prefEditor = pref.edit();
			
			int deathHighScore = pref.getInt(Constants.GAME_RECORD_PREFIX + mGameBuilder.getStageNumber() + Constants.GAME_RECORD_POSTFIX_DEATH, -1);
			long timeHighScore = pref.getLong(Constants.GAME_RECORD_PREFIX + mGameBuilder.getStageNumber() + Constants.GAME_RECORD_POSTFIX_TIME, -1);
			
			if (deathHighScore == -1 && timeHighScore == -1) {
				prefEditor.putInt(Constants.GAME_RECORD_PREFIX + mGameBuilder.getStageNumber() + Constants.GAME_RECORD_POSTFIX_DEATH, clearDead);
				prefEditor.putLong(Constants.GAME_RECORD_PREFIX + mGameBuilder.getStageNumber() + Constants.GAME_RECORD_POSTFIX_TIME, clearTime);
				prefEditor.commit();
			} else {
				if (deathHighScore > clearDead) {
					prefEditor.putInt(Constants.GAME_RECORD_PREFIX + mGameBuilder.getStageNumber() + Constants.GAME_RECORD_POSTFIX_DEATH, clearDead);
					prefEditor.commit();
				}
					
				if (timeHighScore > clearTime) {
					prefEditor.putLong(Constants.GAME_RECORD_PREFIX + mGameBuilder.getStageNumber() + Constants.GAME_RECORD_POSTFIX_TIME, clearTime);
					prefEditor.commit();
				}
			}
			
			new StageClearDialog(mContext).show();
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		mContext = this;
		
		Display display = ((WindowManager)this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		mScreenWidth = display.getWidth();
		mScreenHeight = display.getHeight();
		
		mCallPoint = getIntent().getIntExtra(CALL_POINT, 0);
		mGameBuilder = GameBuilder.getInstance(mScreenWidth, mScreenHeight, getIntent().getIntExtra(GameBuilder.STAGE_NUMBER, 1));
		mGameSurfaceView = new GameSurfaceView(mContext, mGameBuilder);

		setContentView(mGameSurfaceView);
		
		IntentFilter intentFilter = new IntentFilter(Constants.BROADCAST_STAGE_ACTIVITY);
		registerReceiver(mBroadCastReceiverForActivityFinish, intentFilter);
		
		SharedPreferences pref = getSharedPreferences(Constants.GAME_PREFERENCE, Activity.MODE_PRIVATE);
		mGameScreenFlag = pref.getBoolean(Constants.GAME_SCREEN, true);
		
		if (mGameScreenFlag) {
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			mWakeLock = ((PowerManager)getSystemService(Context.POWER_SERVICE)).newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, WAKE_ALWAYS);
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if (!mEventFlag) {
			mEventFlag = true;
			
			mGameSurfaceView.resumeGameEngine();
			
			if (mWakeLock != null)
				mWakeLock.acquire();
		}
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
		if (mEventFlag) {
			mEventFlag = false;
			
			mGameSurfaceView.pauseGameEngine();
			
			if (mWakeLock != null && mWakeLock.isHeld())
				mWakeLock.release();
		}
	}
	
	@Override
	protected void onDestroy() {
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		mGameSurfaceView.stopGameEngine();
		unregisterReceiver(mBroadCastReceiverForActivityFinish);
		
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mGameSurfaceView.isEnterAnimationFinish()) {
			if (mEventFlag) {
				mEventFlag = false;
				
				mGameSurfaceView.stopGameEngine();
				
				switch (mCallPoint) {
				case CALL_POINT_STAGE:
				case CALL_POINT_STAGE_GAME:
					finish();
					break;
				case CALL_POINT_MAIN:
				case CALL_POINT_MAIN_GAME:
					Intent intent = new Intent(GameActivity.this, StageActivity.class);
					startActivity(intent);
					finish();
					break;
				}
			}
		}
	}
	
	private class StageClearDialog extends Dialog {
		private TextView mStageClearTextView = null;
		private boolean mEventFlag = false;
		
		public StageClearDialog(Context context) {
			// TODO Auto-generated constructor stub
			super(context);
			
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.stage_clear_dialog);
			
			mStageClearTextView = (TextView)findViewById(R.id.stageClearTextView);
			
			mStageClearTextView.setOnClickListener(new android.view.View.OnClickListener() {
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (!mEventFlag) {
						mEventFlag = true;
						dismiss();
						
						Intent intent = new Intent(GameActivity.this, GameActivity.class);
						
						switch (mCallPoint) {
						case CALL_POINT_STAGE:
							intent.putExtra(GameActivity.CALL_POINT, GameActivity.CALL_POINT_STAGE_GAME);
							break;
						case CALL_POINT_MAIN:
							intent.putExtra(GameActivity.CALL_POINT, GameActivity.CALL_POINT_MAIN_GAME);
							break;
						case CALL_POINT_STAGE_GAME:
							intent.putExtra(GameActivity.CALL_POINT, GameActivity.CALL_POINT_STAGE_GAME);
							break;
						case CALL_POINT_MAIN_GAME:
							intent.putExtra(GameActivity.CALL_POINT, GameActivity.CALL_POINT_MAIN_GAME);
							break;
						}
						
						intent.putExtra(GameBuilder.STAGE_NUMBER, mGameBuilder.getStageNumber() + 1);
						startActivity(intent);

						finish();
					}
				}
			});
		}
		
		@Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
			if (!mEventFlag) {
				mEventFlag = true;
				dismiss();
				
				switch (mCallPoint) {
				case CALL_POINT_STAGE:
				case CALL_POINT_STAGE_GAME:
					finish();
					break;
				case CALL_POINT_MAIN:
				case CALL_POINT_MAIN_GAME:
					Intent intent = new Intent(GameActivity.this, StageActivity.class);
					startActivity(intent);
					finish();
					break;
				}
			}
		}
	}
}
