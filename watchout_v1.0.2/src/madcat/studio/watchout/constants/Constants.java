package madcat.studio.watchout.constants;

import android.provider.BaseColumns;

public class Constants implements BaseColumns {
	public static final String GAME_VERSION = "Game Version 1.0.2";
	
	//Shared Preference Properties.
	//Begin
	public static final String GAME_PREFERENCE = "GAME_PREFERENCE";

	public static final String GAME_LEVEL = "LEVEL";
	public static final String GAME_SCREEN = "SCREEN";
	public static final String GAME_VIBRATION = "VIBRATION";

	public static final String GAME_SCREEN_ON = "Screen Always ON";
	public static final String GAME_SCREEN_OFF = "Screen Always OFF";
	
	public static final String GAME_VIBRATION_ON = "Vibration ON";
	public static final String GAME_VIBRATION_OFF = "Vibration OFF";
	//End
	
	//Begin
	public static final String GAME_RECORD = "GAME_RECORD";
	
	public static final String GAME_RECORD_PREFIX = "RECORD_";
	
	public static final String GAME_RECORD_POSTFIX_DEATH = "_DEATH";
	public static final String GAME_RECORD_POSTFIX_TIME = "_TIME";
	//End
	
	//Broadcast Receiver Properties.
	//Begin
	public static final String BROADCAST_STAGE_ACTIVITY = "madcat.studio.watchout.game.GameActivity.stageClear";
	//End
	
	//Game Level Properties.
	//Begin
	public static final int GAME_MAX_LEVEL = 20;
	//End
	
	//Game Common String
	//Begin
	public static final String STAGE_STR = "Stage ";
	public static final String DEATH_STR = " Death";
	public static final String HIGHSCORE_STR = "High Score";
	public static final String CURRENTSCORE_STR = "Current Score";
	public static final String GOAL_STR = "GOAL!";
	//End
	
	//Game Stage String
	//Begin
	public static final String STAGE_LOCK_STR = "Lock!";
	//End

	//Game Record String
	//Begin
	public static final String RECORD_STAGE_STR = "'s Record";
	public static final String RECORD_NO_DATA = "NO DATA.";
	//End
}
