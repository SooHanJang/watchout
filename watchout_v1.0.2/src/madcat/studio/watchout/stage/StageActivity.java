package madcat.studio.watchout.stage;

import java.util.ArrayList;

import madcat.studio.watchout.constants.Constants;
import madcat.studio.watchout.game.GameActivity;
import madcat.studio.watchout.game.GameBuilder;
import madcat.studio.watchout.main.R;
import madcat.studio.watchout.utils.Util;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class StageActivity extends Activity {
	private Context mContext = null;
	
	private LinearLayout mStageLayout = null;
	private ImageView mLeftBackground = null;
	private ImageView mRightBackground = null;
	
	private int mScreenWidth = 0;
	private int mScreenHeight = 0;
	
	private Button mBackwardBtn = null;
	private Button mForwardBtn = null;
	
	private ArrayList<Button> mStageBtnList = null;
	
	private int mCurrentLevel = 0;
	private int mWeightLevel = 0;
	
	private boolean mEventFlag = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.stage);
		
		mContext = this;
		
		mStageLayout = (LinearLayout)findViewById(R.id.stageLayout);
		mLeftBackground = (ImageView)findViewById(R.id.stageLeftBackground);
		mRightBackground = (ImageView)findViewById(R.id.stageRightBackground);
		
		mBackwardBtn = (Button)findViewById(R.id.backwardLevel);
		mForwardBtn = (Button)findViewById(R.id.forwardLevel);
		
		mStageBtnList = new ArrayList<Button>();
		
		ButtonEventHandler eventHandler = new ButtonEventHandler();
	
		mBackwardBtn.setOnClickListener(eventHandler);
		mForwardBtn.setOnClickListener(eventHandler);
		
		for (int i = 0; i < 10; i++) {
			Button stageBtn = (Button)findViewById(R.id.stage1+i);
			stageBtn.setOnClickListener(eventHandler);
			mStageBtnList.add(stageBtn);
		}

		Display display = ((WindowManager)this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		mScreenWidth = display.getWidth();
		mScreenHeight = display.getHeight();
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;
		
		Bitmap backgroudImage = BitmapFactory.decodeResource(this.getResources(), R.drawable.background, options);
		backgroudImage = Bitmap.createScaledBitmap(backgroudImage, (int)mScreenWidth, (int)mScreenHeight, true);
		Bitmap leftBackgroundIamge = Bitmap.createBitmap(backgroudImage, 0, 0, backgroudImage.getWidth() / 2, backgroudImage.getHeight());
		Bitmap rightBackgroundIamge = Bitmap.createBitmap(backgroudImage, (int)(mScreenWidth / 2), 0, backgroudImage.getWidth() / 2, backgroudImage.getHeight());
		
		mLeftBackground.setImageBitmap(leftBackgroundIamge);
		mRightBackground.setImageBitmap(rightBackgroundIamge);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if (!mEventFlag) {
			mStageLayout.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.appear_top_down));
			mLeftBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_open));
			mRightBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_open));

			SharedPreferences pref = getSharedPreferences(Constants.GAME_PREFERENCE, Activity.MODE_PRIVATE);
			mCurrentLevel = pref.getInt(Constants.GAME_LEVEL, 0);
			
			for (int i = 0; i < 10; i++) {
				Button stageBtn = mStageBtnList.get(i);
				if (i + mWeightLevel > mCurrentLevel)
					stageBtn.setText(Constants.STAGE_LOCK_STR);
				else
					stageBtn.setText(Constants.STAGE_STR + (i + mWeightLevel + 1));
			}
			
			new StageTask().execute(StageTask.ACTIVITY_RESUME);
		}
	}
	
	@Override
	protected void onDestroy() {
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mEventFlag) {
			mStageLayout.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.disappear_top_down));
			mLeftBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_close));
			mRightBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_close));
			
			mEventFlag = false;
			
			mBackwardBtn.setClickable(false);
			mForwardBtn.setClickable(false);
			
			for (Button stageBtn : mStageBtnList)
				stageBtn.setClickable(false);
			
			new StageTask().execute(StageTask.BACK_KEY);
		}
	}
	
	public void refreshStageLevel() {
		SharedPreferences pref = getSharedPreferences(Constants.GAME_PREFERENCE, Activity.MODE_PRIVATE);
		mCurrentLevel = pref.getInt(Constants.GAME_LEVEL, 0);
		
		if (!mEventFlag) {
			mEventFlag = true;
			
			for (int i = 0; i < 10; i++) {
				Button stageBtn = mStageBtnList.get(i);
				stageBtn.setClickable(true);
				
				if (i + mWeightLevel > mCurrentLevel)
					stageBtn.setText(Constants.STAGE_LOCK_STR);
				else
					stageBtn.setText(Constants.STAGE_STR + (i + mWeightLevel + 1));
			}		
		}
	}
	
	private class ButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			// TODO Auto-generated method stub
			int btnNumber = v.getId() - mStageBtnList.get(0).getId();
			
			if (mEventFlag) {
				mEventFlag = false;

				switch (v.getId()) {
				case R.id.backwardLevel:
					if (mWeightLevel != 0) {
						mWeightLevel -= 10;
						refreshStageLevel();
					} else {
						mEventFlag = true;
					}
					break;
				case R.id.forwardLevel:
					if (mWeightLevel != Constants.GAME_MAX_LEVEL - 10) {
						mWeightLevel += 10;
						refreshStageLevel();
					} else {
						mEventFlag = true;
					}
					break;
				case R.id.stage1:
				case R.id.stage2:
				case R.id.stage3:
				case R.id.stage4:
				case R.id.stage5:
				case R.id.stage6:
				case R.id.stage7:
				case R.id.stage8:
				case R.id.stage9:
				case R.id.stage10:
					if (mCurrentLevel < btnNumber + mWeightLevel) {
						mEventFlag = true;
						new StageLockDialog(mContext).show();
					} else {
						mStageLayout.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.disappear_top_down));
						mLeftBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_close));
						mRightBackground.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_close));
						new StageTask().execute(StageTask.START_GAME, btnNumber + mWeightLevel + 1);
					}
					break;
				}	
			}
		}
	}
	
	private class StageTask extends AsyncTask<Integer, Void, Void> {
		private static final int WAITING_TIME = 1500;
		
		public static final int ACTIVITY_RESUME = 0;
		
		public static final int BACK_KEY = 2;
		public static final int START_GAME = 3;
		
		@Override
		protected Void doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			try {
				Thread.sleep(WAITING_TIME);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			switch (params[0]) {
			case ACTIVITY_RESUME:
				mEventFlag = true;
				mBackwardBtn.setClickable(true);
				mForwardBtn.setClickable(true);
				for (Button stageBtn : mStageBtnList)
					stageBtn.setClickable(true);
				break;
			case BACK_KEY:
				finish();
				break;
			case START_GAME:
				Intent intent = new Intent(StageActivity.this, GameActivity.class);
				intent.putExtra(GameActivity.CALL_POINT, GameActivity.CALL_POINT_STAGE);
				intent.putExtra(GameBuilder.STAGE_NUMBER, params[1]);
				startActivity(intent);
				break;
			}

			return null;
		}
	}
	
	private class StageLockDialog extends Dialog {
		private TextView mStageLockTextView = null;
		private boolean mEventFlag = false;
		
		public StageLockDialog(Context context) {
			// TODO Auto-generated constructor stub
			super(context);
			
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.stage_lock_dialog);
			
			mStageLockTextView = (TextView)findViewById(R.id.stageLockTextView);
			
			mStageLockTextView.setOnClickListener(new android.view.View.OnClickListener() {
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (!mEventFlag) {
						mEventFlag = true;
						dismiss();
					}
				}
			});
		}
		
		@Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
			if (!mEventFlag) {
				mEventFlag = true;
				dismiss();
			}
		}
	}
}
